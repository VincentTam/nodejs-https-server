# A minimal example Node.js HTTPS server on localhost

## Objective

Test Node.js apps with HTTPS locally.

## How to use?

- Start
    1. Grant the execution permission to the shell script
    `gen-prkey-and-cert.sh`.
    2. Run the shell script to generate a self-signed certificate.
    3. Run the javascript `index.js` in the terminal.
    4. Bypass the [`ERROR_SELF_SIGNED_CERT` warning][1] in Firefox.

            $ chmod +x gen-prkey-and-cert.sh
            $ ./gen-prkey-and-cert.sh
            $ node index.js

- Stop: press `<Ctrl-C>` to stop the server.

## Source

https://stackoverflow.com/a/21809393/3184351

## License

The original author of the code chose the MIT license, so I have to follow suit.

[1]: https://support.mozilla.org/en-US/kb/error-codes-secure-websites?as=u&utm_source=inproduct#w_self-signed-certificate